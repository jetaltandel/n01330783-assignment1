﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace passionProject.Models
{
    public class Movies
    {
        // Movie property
        [Key]
        public int MovieID { get; set; }

        [Required, StringLength(250), Display(Name = "Title")]
        public string MovieName { get; set; }
        //movie has actor
        public virtual Actor actor { get; set; }
    }
}