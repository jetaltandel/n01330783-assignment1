﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace passionProject.Models
{
    public class Actor
    {
        //actor property
        [Key]
        public int ActorID { get; set; }

        [Required, StringLength(150), Display(Name = "FirstName")]
        public string ActorFName { get; set; }

        [Required, StringLength(150), Display(Name = "LastName")]
        public string ActorLName { get; set; }
         // one actor have many movies
        public virtual ICollection<Movies> Movie { get; set; }
    }



}
