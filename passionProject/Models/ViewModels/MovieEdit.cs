﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace passionProject.Models.ViewModels
{
    public class MovieEdit
    {
        public MovieEdit()
        {

        }
        public virtual Movies movie { get; set; }
        public IEnumerable<Actor> actors { get; set; }
    }
}