﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;
using passionProject.Models;
using passionProject.Models.ViewModels;
using System.Diagnostics;

namespace passionProject.Controllers
{
    public class MoviesController : Controller
    {
        private ActorCMSContext db = new ActorCMSContext();

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        public ActionResult New()
        {

            return View(db.Actors.ToList());
        }
        [HttpPost]
        public ActionResult Create(string MovieName_New, int? MovieActor_New)
        {
            string query = "insert into Movies (MovieName, actor_ActorID) " +
                "values (@name,@aid)";

            SqlParameter[] myparams = new SqlParameter[2];
            myparams[0] = new SqlParameter("@name", MovieName_New);
            myparams[1] = new SqlParameter("@aid", MovieActor_New);

            db.Database.ExecuteSqlCommand(query, myparams);
            Debug.WriteLine(query);
            return RedirectToAction("List");
        }
        public ActionResult Show(int id)
        {
            string query = "select * from movies where movieid =@id";

           

            Debug.WriteLine(query);
           

            return View(db.Movie.SqlQuery(query, new SqlParameter("@id", id)).FirstOrDefault());
        }
        public ActionResult Edit(int? id)
        {
            
            MovieEdit movieedit = new MovieEdit();
            movieedit.movie = db.Movie.Find(id);
            movieedit.actors = db.Actors.ToList();
            
            if (movieedit.movie != null) return View(movieedit);
            else return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Edit(string MovieName, int? MovieActor, int? id)
        {
            if ((id == null) || (db.Movie.Find(id) == null))
            {
                return HttpNotFound();

            }
            
            string query = "update movies set moviename=@name, " +
                "actor_ActorID=@aid where movieid = @id";
            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter();
            myparams[0].ParameterName = "@name";
            myparams[0].Value = MovieName;

            myparams[1] = new SqlParameter();
            myparams[1].ParameterName = "@aid";
            myparams[1].Value = MovieActor;

            myparams[2] = new SqlParameter();
            myparams[2].ParameterName = "@id";
            myparams[2].Value = id;


            db.Database.ExecuteSqlCommand(query, myparams);
            return RedirectToAction("Show/" + id);
        }
        public ActionResult Delete(int id)
        {
            


            string query = "delete from Movie where movieid = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));


            return RedirectToAction("List");
        }

        public ActionResult List()
        {

            
            return View(db.Movie.ToList());

        }
    }


  }
