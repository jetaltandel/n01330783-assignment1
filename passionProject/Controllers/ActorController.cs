﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;
using passionProject.Models;
using passionProject.Models.ViewModels;
using System.Diagnostics;

namespace passionProject.Controllers
{
    public class ActorController : Controller
    {
        private ActorCMSContext db = new ActorCMSContext();
        
        // GET: Actor
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            
            IEnumerable <Actor> actors = db.Actors.ToList();
            return View(actors);
        }
        //get : create actor
        public ActionResult create()
        {
            

            ActorEdit actoreditview = new ActorEdit();
            return View(actoreditview);
        }


        [HttpPost]
        public ActionResult create(string ActorFName_New, string ActorLName_New)
        {
            // Basic query   
            string query = "insert into actors (ActorFName,ActorLName ) values (@firstname, @lastname)";
            SqlParameter[] myparams = new SqlParameter[2];
            myparams[0] = new SqlParameter("@firstname", ActorFName_New);
            myparams[1] = new SqlParameter("@bio", ActorLName_New);
            
           
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }



        public ActionResult Edit(int id)
        {
            //For edit we need a list of actors 
            

            ActorEdit actoreditview = new ActorEdit();
            
            actoreditview.actor = db.Actors.Find(id);

            return View(actoreditview);
        }
        [HttpPost]
        public ActionResult Edit(int id, string ActorFName, string ActorLName)
        {
            //If the ID doesn't exist or the actor doesn't exist
            if ((id == null) || (db.Actors.Find(id) == null))
            {
                return HttpNotFound();

            }
            string query = "update actors set ActorFName=@firstname, ActorLName=@lastname where actorid=@id";
            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@firstname", ActorFName);
            myparams[1] = new SqlParameter("@lastname", ActorLName);
            myparams[3] = new SqlParameter("@id", id);
            
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Show/" + id);
        }
        public ActionResult Show(int? id)
        {
            //If the id doesn't exist or the actor doesn't exist
            if ((id == null) || (db.Actors.Find(id) == null))
            {
                return HttpNotFound();

            }
            string query = "select * from actors where actorid=@id";
            
            SqlParameter[] myparams = new SqlParameter[1];
            myparams[0] = new SqlParameter("@id", id);

            
            Actor myactor = db.Actors.SqlQuery(query, myparams).FirstOrDefault();

            return View(myactor);
        }
        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Actors.Find(id) == null))
            {
                return HttpNotFound();

            }
           // delete associated movies
           string query = "delete from Movies where actor_ActorID=@id";
           SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);

            // delete actor
            query = "delete from Actors where actorsid=@id";
            param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            return RedirectToAction("List");
        }



        
    }
}